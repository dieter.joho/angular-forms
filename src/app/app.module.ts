import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SimpleFormComponent} from './simple-form/simple-form.component';
import {BuilderFormComponent} from './builder-form/builder-form.component';

@NgModule({
  declarations: [
    AppComponent,
    SimpleFormComponent,
    BuilderFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
